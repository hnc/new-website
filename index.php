<?php
	
	// Library
	require_once('include/include.php');
	require_once('lib/lib.php');
	require_once(data_or_demo() . '/include.php');
	
	// First and foremost include a file to allow HTTP header modifications -->
	include(data_or_demo() . '/first_and_foremost.php');
	
	// Root path
	$root_path = dirname(__FILE__); // http://stackoverflow.com/questions/1882044/get-parent-directory-of-running-script/1882059#1882059
?>

<!DOCTYPE html>
<html lang="fr">
	
	<head>
		<meta charset="utf-8">
		<title><?php website_title(); ?></title>
		<meta name="keywords" content="<?php website_keywords(); ?>">
		<meta name="description" content="<?php website_description(); ?>">
		<meta name="author" content="<?php website_author(); ?>">
		<link rel="shortcut icon" type="image/png" href="<?php echo data_or_demo(); ?>/favicon.png">
		<link rel="stylesheet" href="include/css.css" type="text/css" media="screen">
	</head>
	
	<body>
		
		<header>
			<?php include(data_or_demo() . '/header.php'); ?>
		</header>
		
		<header>
			<?php include('include/header_menu.php'); ?>
		</header>
		
		<div>
			<aside>
				<nav>
					<?php include('include/menu.php'); ?>
				</nav>
			</aside>
			
			<?php
				
				// Page "logos"
				if (isset($_GET['section']) == false && isset($_GET['page']) && $_GET['page'] == 'logos')
				{
					include('img/logos/include.php');
					echo '<h1>' . img_txt_code('img/48x48/' . $page_img, $page_title) . '</h1>';
					include('img/logos/article.php');
				}
				// User's section & page
				else
				{
					$section = isset($_GET['section']) ? $_GET['section'] : 'main';
					$section_path = get_section_path($section);
					$pages = get_pages($section_path);
					$page_path = isset($_GET['page']) ? get_page_path($_GET['page']) : '';
					
					// Section title
					if ($section_path != '')
					{
						include($section_path . '/include.php');
						echo '<h1>' . img_txt_code('img/48x48/' . $section_img, $section_title) . '</h1>';
					}
					
					// Section error
					if ($section_path === '')
					{
						echo '<article>';
						error('Section "' . htmlentities($_GET["section"]) . '" does no exist.');
						echo '</article>';
						include(data_or_demo() . '/00_main/article.php');
					}
					
					// Section article
					if ($section_path != '')
					{
						include($section_path . '/article.php');
					}
					
					// Pages
					if (isset($_GET['page']) === false)
					{
						foreach ($pages as $page)
						{
							if (file_exists($section_path . '/' . $page . '/include.php'))
							{
								include ($section_path . '/' . $page . '/include.php');
								img_url_description('img/48x48/' . $page_img, local_link($section, substr($page, 3)), $page_title, $page_description);
							}
						}
					}
					
					// Page article
					if ($page_path != '' && file_exists($page_path . '/include.php') && file_exists($page_path . '/article.php'))
					{
						include($page_path . '/include.php');
						echo '<h1>' . img_txt_code('img/48x48/' . $page_img, $page_title) . '</h1>';
						include($page_path . '/article.php');
					}
					
					// Page error
					if (isset($_GET['page']) && $page_path === '')
					{
						echo '<article>';
						error('Page "' . htmlentities($_GET['page']) . '" does no exist.</error>');
						echo '</article>';
					}
					
					// Pages
					if (isset($_GET['page']))
					{
						echo '<br>';
						img_txt('img/32x32/newspaper.png', '<b>Other pages:</b>'); echo '<br>';
						foreach ($pages as $page)
						{
							if (file_exists($section_path . '/' . $page . '/include.php'))
							{
								include ($section_path . '/' . $page . '/include.php');
								echo '<table><tr><td>';
								img_txt('img/32x16/' . $page_img, url_code(local_link($section, substr($page, 3)), $page_title) . ' <i>(' . $page_description . ')<i>');
								echo '</td></tr></table>';
							}
						}
						echo '<br>';
					}
				}
				
			?>
		</div>
		
		<!-- footers -->
		<?php
			$files = scandir(data_or_demo(), 0);
			
			foreach ($files as $file)
			{
				$path_parts = pathinfo($file);
				
				if (isset($path_parts['extension']) && $path_parts['extension'] === 'php' && strpos($path_parts['filename'], 'footer') !== false)
				{
					echo '<footer>';
					include(data_or_demo() . '/' . $file);
					echo '</footer>';
				}
			}
		?>
		
	</body>
	
</html>
