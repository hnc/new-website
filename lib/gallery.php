<?php
	
	function gallery($dir)
	{
		$files = scandir($dir, 0);
		
		foreach ($files as $i => $e)
		{
			$path = $dir . '/' . $e;
			
			if ($e === '.' or $e === '..' or is_dir($path)) { continue; }
			
			$img = $dir . '/quality_75/' . $e;
			if (file_exists($img) == false or getimagesize($img) == false)
			{
				$img = $path;
			}
			
			$thumbnails = $dir . '/thumbnails/' . $e;
			if (file_exists($thumbnails) == false or getimagesize($thumbnails) == false)
			{
				$thumbnails = $img;
			}
			
			$size = getimagesize($thumbnails);
			
			if ($size != false)
			{
				$width = 'auto';
				$height = 'auto';
				
				if ($size[0] > $size[1]) { $width = 128; }
				else { $height = 128; }
				
				$url = $img;
				
				echo '<div align="center" style="margin: 10px; padding: 0px; width: 130px; height: 130px; line-height:130px; background-color: white; box-shadow: 1px 1px 12px #555; display: inline-block">';
					url($url, img_code($thumbnails, $height, $width), 'Click to enlarge');
				echo '</div>';
			}
		}
	}
	
?>
