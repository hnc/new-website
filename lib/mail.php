<?php
	
	function is_mail_address($mail)
	{
		if
		(
			empty($mail) == false &&
			preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#', $mail)
		)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function send_mail_html($title, $message, $receiver_mail, $transmitter_name, $transmitter_mail)
	{
		if(!is_mail_address($receiver_mail) || !is_mail_address($transmitter_mail))
		{
			return false;
		}
		else
		{
			$body = '<html><head><title>'.$title.'</title></head><body>';
			$body .= '<p>Hello, you received a message from your website.</p>';
			$body .= '<p><strong>Name</strong>: '.$transmitter_name.'</p>';
			$body .= '<p><strong>Email</strong>: '.$transmitter_mail.'</p>';
			$body .= "<p><strong>Message</strong>:</p>";
			$body .= "======== ";
			$body .= "<p>".$message."</p>";
			$body .= "======== ";
			$body .= "<p>Posted on ".date("d/m/Y - H:i")."</p>";
			$body .= '</body></html>'; // Contenu du message de l'email (en XHTML)	

			// Pour envoyer un email HTML, l'en-tête Content-type doit être défini
			$headers = 'MIME-Version: 1.0'."\r\n";
			$headers .= 'Content-type: text/html; charset=UTF-8'."\r\n";
			$headers .= 'Content-Transfer-Encoding: 8bit';

			return mail($receiver_mail, $title, $body, $headers);
		}
	}

?>
