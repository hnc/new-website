<article>
	
	<?php
		
		$current_dir = '.' . str_replace(getcwd(), '', __FILE__);
		$current_dir = str_replace('/' . basename($current_dir), '', $current_dir);
		
	?>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/cmake.png', '<b>CMake (CC BY 2.0)</b>',
			url_code($current_dir . '/logos/cmake.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/cmake.License.CC_BY_2.0.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/code_blocks.png', '<b>Code::Blocks</b>',
			'without name: ' .
			url_code($current_dir . '/logos/code_blocks.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/code_blocks.License.txt', 'license file')
			. ', with name: ' .
			url_code($current_dir . '/logos/code_blocks_with_name.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/code_blocks.License.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/creative_commons.png', '<b>Creative Commons (CC BY-SA 4.0)</b>',
			url_code($current_dir . '/logos/creative_commons.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/creative_commons.License.CC_BY-SA_4.0.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/debian.png', '<b>Debian (CC BY-SA 3.0)</b>',
			'without name: ' .
			url_code($current_dir . '/logos/debian.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/debian.License.CC_BY-SA_3.0.txt', 'license file')
			. ', with name: ' .
			url_code($current_dir . '/logos/debian_with_name.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/debian_with_name.License.CC_BY-SA_3.0.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/fete_de_la_science.png', '<b>Fête De La Science</b>',
			url_code($current_dir . '/logos/fete_de_la_science.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/firefox.License.Trademark.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/firefox.png', '<b>Mozilla Firefox (Trademark)</b>',
			url_code($current_dir . '/logos/firefox.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/firefox.License.Trademark.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/git.png', '<b>Git (CC BY 3.0)</b>',
			'without name: ' .
			url_code($current_dir . '/logos/git.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/git.License.CC_BY_3.0.txt', 'license file')
			. ', with name: ' .
			url_code($current_dir . '/logos/git_with_name.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/git_with_name.License.CC_BY_3.0.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/github.png', '<b>GitHub</b>',
			url_code($current_dir . '/logos/github.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/github.License.pdf', 'license file')
		);
	?>
	<?php
		img_txt_description
		(
			'img/48x48/github_octocat.png', '<b>GitHub Octocat</b>',
			url_code($current_dir . '/logos/github_octocat.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/github_octocat.License.pdf', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/gitlab.png', '<b>GitLab</b>',
			url_code($current_dir . '/logos/gitlab.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/gitlab.License.pdf', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/gitorious.png', '<b>Gitorious (CC BY-ND 3.0)</b>',
			'without name: ' .
			url_code($current_dir . '/logos/gitorious.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/gitorious.License.CC_BY-ND_3.0.txt', 'license file')
			. ', with name: ' .
			url_code($current_dir . '/logos/gitorious_with_name.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/gitorious_with_name.License.CC_BY-ND_3.0.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/gnu.png', '<b>GNU (Free Art License 1.3)</b>',
			url_code($current_dir . '/logos/gnu.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/gnu.License.Free_Art_License_1.3.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/hnc_northern_pintail.png', '<b>hnc - Northern Pintail (Canard Pilet) (CC BY-SA 3.0)</b>',
			url_code($current_dir . '/logos/hnc_northern_pintail.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/hnc_northern_pintail.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/hnc_northern_pintail_300x300.png', 'png (300x300)') . ' | ' .
			url_code($current_dir . '/logos/hnc_northern_pintail.License.CC_BY-SA_3.0.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/iceweasel.png', '<b>Iceweasel (GNU GPL v2)</b>',
			url_code($current_dir . '/logos/iceweasel.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/iceweasel.License.GNU_GPL_v2.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/lri.jpg', '<b>LRI</b>',
			'without name: ' .
			url_code($current_dir . '/logos/lri.jpg', 'jpg') . ' | ' .
			url_code($current_dir . '/logos/lri.License.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/macports.png', '<b>MacPorts (BSD)</b>',
			'without name: ' .
			url_code($current_dir . '/logos/macports.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/macports_183x183.png', 'png (183x183)') . ' | ' .
			url_code($current_dir . '/logos/macports.License.BSD.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/notepadpp.png', '<b>Notepad++ (CC BY-SA 3.0)</b>',
			'without name: ' .
			url_code($current_dir . '/logos/notepadpp.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/notepadpp.License.CC_BY_3.0.txt', 'license file')
			. ', with name: ' .
			url_code($current_dir . '/logos/notepadpp_with_name.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/notepadpp_with_name.License.CC_BY_3.0.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/ogre.png', '<b>OGRE</b>',
			'without name: ' .
			url_code($current_dir . '/logos/ogre.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/ogre.License.pdf', 'license file')
			. ', with name: ' .
			url_code($current_dir . '/logos/ogre_with_name.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/ogre_with_name.License.pdf', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/openscop.png', '<b>OpenScop (CC BY 3.0)</b>',
			url_code($current_dir . '/logos/openscop.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/openscop_300x300.png', 'png (300x300)') . ' | ' .
			url_code($current_dir . '/logos/openscop.License.CC_BY_3.0.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/python.png', '<b>Python (GNU GPL v2)</b>',
			url_code($current_dir . '/logos/python.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/python.License.GNU_GPL_v2.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/qt_creator.png', '<b>Qt Creator</b>',
			'without name: ' .
			url_code($current_dir . '/logos/qt_creator.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/qt_creator.License.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/sfml2.png', '<b>SFML 2 (CC0 1.0)</b>',
			'without name: ' .
			url_code($current_dir . '/logos/sfml2.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/sfml2.License.CC0_1.0.txt', 'license file')
			. ', with name: ' .
			url_code($current_dir . '/logos/sfml2_with_name.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/sfml2_with_name.License.CC0_1.0.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/toile_libre.png', '<b>Toile-Libre.org</b>',
			url_code($current_dir . '/logos/toile_libre.png', 'png') . ' | ' .
			url_code($current_dir . '/logos/toile_libre.License.txt', 'license file')
		);
	?>
	</p>
	
	<p>
	<?php
		img_txt_description
		(
			'img/48x48/wikipedia.png', '<b>Wikipedia</b>',
			url_code($current_dir . '/logos/wikipedia.svg', 'svg') . ' | ' .
			url_code($current_dir . '/logos/wikipedia.License.pdf', 'license file')
		);
	?>
	</p>
	
</article>
