<?php
	
	echo '<table>';
	
	$sections = get_sections_or_pages(data_or_demo());
	
	$nb_section = 0;
	$section_id_current = -1;
	
	echo '<tr>';
	foreach ($sections as $section)
	{
		$section_include_file = data_or_demo() . '/' . $section . '/include.php';
		
		if (file_exists($section_include_file))
		{
			include($section_include_file);
			
			if ($section_main)
			{
				echo '<td>';
				
				echo '<table><tr><td>';
				img_url('img/32x32/' . $section_img, local_link(substr($section, 3)), $section_title);
				echo '</td></tr></table>';
				
				echo '</td>';
				
				if (isset($_GET['section']) && $_GET['section'] == substr($section, 3))
				{
					$section_id_current = $nb_section;
				}
				$nb_section++;
			}
		}
	}
	echo '</tr>';
	
	echo '<tr>';
	for ($i = 0; $i < $nb_section; $i++)
	{
		if ($i === $section_id_current)
		{
			echo '<td class="menu_background_color">';
		}
		else
		{
			echo '<td>';
		}
		
		echo '</td>';
	}
	echo '</tr>';
	
	echo '</table>';
	
?>
