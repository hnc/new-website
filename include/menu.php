<?php
	
	$sections = get_sections_or_pages(data_or_demo());
	
	foreach ($sections as $section)
	{
		$section_include_file = data_or_demo() . '/' . $section . '/include.php';
		
		if (file_exists($section_include_file))
		{
			include($section_include_file);
			
			if ($section_secondary)
			{
				echo '<table><tr>';
				
				if (isset($_GET['section']) && $_GET['section'] == substr($section, 3))
				{
					echo '<td class="menu_background_color">';
				}
				else
				{
					echo '<td>';
				}
				echo '</td>';
				
				echo '<td>';
				img_url('img/32x32/' . $section_img, local_link(substr($section, 3)), $section_title);
				echo '</td>';
				
				echo '</tr></table>';
				
				$section_path = get_section_path(substr($section, 3));
				$pages = get_pages($section_path);
				
				foreach ($pages as $page)
				{
					include ($section_path . '/' . $page . '/include.php');
					if ($page_link_in_menu)
					{
						echo '<table><tr><td>';
						img_url('img/32x16/' . $page_img, local_link(substr($section, 3), substr($page, 3)), $page_title);
						echo '</td></tr></table>';
					}
				}
			}
		}
	}
	
?>
