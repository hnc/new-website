<?php
	
	function data_or_demo()
	{
		if (file_exists('data/include.php'))
		{
			return 'data';
		}
		else
		{
			return 'data_demo';
		}
	}
	
	function files_or_demo()
	{
		if (data_or_demo() === 'data')
		{
			return 'files';
		}
		else
		{
			return 'files_demo';
		}
	}
	
	function local_link($section, $page = '')
	{
		if ($page === '')
		{
			return './index.php?section=' . $section;
		}
		else
		{
			return './index.php?section=' . $section . '&amp;' . 'page=' . $page;
		}
	}
	
	function local_link_file($section, $file_or_page, $file = '')
	{
		if ($file === '')
		{
			$page = '';
			$file = $file_or_page;
		}
		else
		{
			$page = $file_or_page;
			$file = $file;
		}
		
		$files_dir = files_or_demo();
		
		if ($page === '')
		{
			return './' . $files_dir . '/' . $section . '/' . $file;
		}
		else
		{
			return './' . $files_dir . '/' . $section . '/' . $page . '/' . $file;
		}
	}
	
	function get_sections_or_pages($section_dirname)
	{
		$r = array();
		
		if ($section_dir = opendir($section_dirname))
		{
			while (($file = readdir($section_dir)) !== false)
			{
				if ($file != '.' && $file != '..')
				{
					if (is_dir($section_dirname . '/' . $file))
					{
						array_push($r, $file);
					}
				}
			}
		}
		
		sort($r);
		
		return $r;
	}
	
	function get_section_path($section)
	{
		$r = '';
		
		if ($section)
		{
			$sections = get_sections_or_pages(data_or_demo());
			
			foreach ($sections as $i)
			{
				if ($section === substr($i,3))
				{
					$r = data_or_demo() . '/' . $i;
					break;
				}
			}
		}
		
		return $r;
	}
	
	function get_pages($section_path)
	{
		return get_sections_or_pages($section_path);
	}
	
	function get_page_path($page)
	{
		$r = '';
		
		if ($page)
		{
			$section_path = get_section_path($_GET['section']);
			$pages = get_pages($section_path);
			
			foreach ($pages as $i)
			{
				if ($page === substr($i,3))
				{
					$r = $section_path . '/' . $i;
					break;
				}
			}
		}
		
		return $r;
	}
	
?>
