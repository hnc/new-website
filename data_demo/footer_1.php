<small>

Copyright © 2013,2014 Lénaïc <?php small_caps('Bagnères'); ?>, hnc@singularity.fr & David <?php small_caps('Guinehut'); ?>, dvdg@singularity.fr | Texts in <?php img_url('img/16x16/creative_commons.png', 'http://creativecommons.org/licenses/by-sa/3.0/', 'CC BY-SA 3.0', 'Creative Common Attribution-ShareAlike 3.0 License'); ?>

</small>
