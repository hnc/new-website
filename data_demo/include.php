<?php
	
	function website_title() { echo 'Demo of new-website'; }
	
	function website_keywords() { echo 'website, PHP, CMS, HTML'; }
	
	function website_description() { echo 'Website of new-website CMS'; }
	
	function website_author() { echo 'Lénaïc BAGNÈRES & David GUINEHUT'; }
	
?>
