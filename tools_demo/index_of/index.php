<!-- This file list directories and files of a directory -->

<!-- https://gitlab.com/hnc/new-website -->


<!DOCTYPE html>
<html lang="fr">
	
	<?php $title = 'Index of ' . $_SERVER['REQUEST_URI']; ?>
	
	<head>
		<meta charset="utf-8">
		<title><?php echo $title; ?></title>
	</head>
	
	<body>
		
		<h1><?php echo $title; ?></h1>
		
		<table>
		
		<tr>
			
			<th> <b> Name </b> </th>
			<th> <b> Last modified </b> </th>
			<th> <b> Size </b> </th>
			
		</tr>
		
		<?php
		
		$files = scandir('.', 1);
		
		foreach ($files as $file)
		{
			$path_parts = pathinfo($file);
			
			if ($file === '.' || $path_parts['extension'] === 'php')
			{
				// Do nothing
			}
			else
			{
				echo '<tr>';
				
				echo '<td> <a href="' . $file . '">' . $file . '</a> &nbsp;&nbsp; </td>';
				echo '<td>' . date('Y-m-d H:i.', filemtime($file)) . ' &nbsp;&nbsp; </td>';
				echo '<td>' . number_format(filesize($file) / 1024 / 1024, 2) . 'Mio </td>';
				
				echo '</tr>';
			}
		}
		
		?>
		
		</table>
		
	</body>
	
</html>
