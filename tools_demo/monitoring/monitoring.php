<?php

	// From lib/ip.php
	function monitoring_ip_client()
	{
		if (empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
		{
			return $_SERVER["REMOTE_ADDR"];
		}
		else
		{
			return $_SERVER["HTTP_X_FORWARDED_FOR"];
		}
	}
	
	// Display log.txt
	function monitoring_log_display($filename = 'monitoring.log')
	{
		$computer_names = array();
		$computers = array();
		
		if (file_exists($filename))
		{
			$file = fopen($filename, 'r');
			
			$timestamp = '';
			$name = '';
			$local_ip = '';
			$cpu = '';
			$ram = '';
			$uptime = '';
			$system = '';
			$os = '';
			$gpu = '';
			$desc = '';
			$pwd_user = '';
			$ip = '';
			
			while (!feof($file))
			{
				$line = fgets($file);
				$line = substr($line, 0, strlen($line) - 1);
				
				if (substr($line, 0, 12) === '# timestamp:' || feof($file))
				{
					# Save previous
					if ($timestamp !== '')
					{
						if (in_array($name, $computer_names) == false)
						{
							array_push($computer_names, $name);
						}
						
						$key = array_search($name, $computer_names);
						
						if (isset($computers[$key]) == false || $computers[$key][1] <= $timestamp)
						{
							$computers[$key] = array();
							$computers[$key][0] = $local_ip;
							$computers[$key][1] = $timestamp;
							$computers[$key][2] = $cpu;
							$computers[$key][3] = $ram;
							$computers[$key][4] = $uptime;
							$computers[$key][5] = $system;
							$computers[$key][6] = $os;
							$computers[$key][7] = $gpu;
							$computers[$key][8] = $desc;
							$computers[$key][9] = $pwd_user;
							$computers[$key][10] = $ip;
						}
					}
					
					# Start new
					$timestamp = date('d/m/Y H:m:s', intval(substr($line, 12)));
					$name = '';
					$local_ip = '';
					$cpu = '';
					$ram = '';
					$uptime = '';
					$system = '';
					$os = '';
					$gpu = '';
					$desc = '';
					$pwd_user = '';
					$ip = '';
				}
				else if (substr($line, 0, 5) === 'name:')
				{
					$name = substr($line, 5);
				}
				else if (substr($line, 0, 9) === 'local ip:')
				{
					$local_ip = substr($line, 9);
				}
				else if (substr($line, 0, 4) === 'cpu:')
				{
					$cpu = substr($line, 4);
				}
				else if (substr($line, 0, 4) === 'ram:')
				{
					$ram = substr($line, 4);
				}
				else if (substr($line, 0, 7) === 'uptime:')
				{
					$uptime = substr($line, 7);
				}
				else if (substr($line, 0, 7) === 'system:')
				{
					$system = substr($line, 7);
				}
				else if (substr($line, 0, 3) === 'os:')
				{
					$os = substr($line, 3);
				}
				else if (substr($line, 0, 4) === 'gpu:')
				{
					$gpu = substr($line, 4);
				}
				else if (substr($line, 0, 5) === 'desc:')
				{
					$desc = substr($line, 5);
				}
				else if (substr($line, 0, 9) === 'pwd_user:')
				{
					$pwd_user = substr($line, 9);
				}
				else if (substr($line, 0, 3) === 'ip:')
				{
					$ip = substr($line, 3);
				}
			}
			
			fclose($file);
		}
		
		if (empty($computers))
		{
			echo 'No monitoring log.';
		}
		else
		{
			$first_display = true;
			
			foreach ($computer_names as $key => $name)
			{
				if ($computers[$key][9] === '' || (isset($_GET['pwd']) && $_GET['pwd'] ==$computers[$key][9]))
				{
					if ($first_display === false) { echo '<br>'; }
					else { $first_display = false; }
					echo '<b>' . $name . '</b> (' . $computers[$key][1] . ')<br>';
					if ($computers[$key][0] !== '') { echo '<b>Local IP:</b> ' . $computers[$key][0] . '<br>'; }
					if ($computers[$key][2] !== '') { echo '<b>CPU:</b> ' . $computers[$key][2] . '<br>'; }
					if ($computers[$key][3] !== '') { echo '<b>RAM:</b> ' . $computers[$key][3] . '<br>'; }
					if ($computers[$key][4] !== '') { echo '<b>Uptime:</b> ' . $computers[$key][4] . '<br>'; }
					if ($computers[$key][5] !== '') { echo '<b>System:</b> ' . $computers[$key][5] . '<br>'; }
					if ($computers[$key][6] !== '') { echo '<b>System:</b> ' . $computers[$key][6] . '<br>'; }
					if ($computers[$key][7] !== '') { echo '<b>GPU:</b> ' . $computers[$key][7] . '<br>'; }
					if ($computers[$key][8] !== '') { echo '<b>Description:</b> ' . $computers[$key][8] . '<br>'; }
				}
			}
		}
	}
	
	// Save log
	function monitoring_log($password, $filename = 'monitoring.log')
	{
		if (isset($_GET['name']) === false)
		{
			//echo 'Error: monitoring_log: name is not set';
		}
		else if ($_GET['name'] === '')
		{
			echo 'Error: monitoring_log: name is empty';
		}
		else if (isset($_GET['pwd']) === false)
		{
			//echo 'Error: monitoring_log: pwd is not set';
		}
		else if ($_GET['pwd'] !== $password)
		{
			echo 'Error: monitoring_log: pwd is not correct';
		}
		else
		{
			$name = $_GET['name'];
			$local_ip = isset($_GET['local_ip']) ? $_GET['local_ip'] : '';
			$cpu = isset($_GET['cpu']) ? $_GET['cpu'] : '';
			$ram = isset($_GET['ram']) ? $_GET['ram'] : '';
			$uptime = isset($_GET['uptime']) ? $_GET['uptime'] : '';
			$system = isset($_GET['system']) ? $_GET['system'] : '';
			$os = isset($_GET['os']) ? $_GET['os'] : '';
			$gpu = isset($_GET['gpu']) ? $_GET['gpu'] : '';
			$desc = isset($_GET['desc']) ? $_GET['desc'] : '';
			$pwd_user = isset($_GET['pwd_user']) ? $_GET['pwd_user'] : '';
			$ip = monitoring_ip_client();
			
			$date = date_create();
			
			$txt = '# timestamp:' . $date->getTimestamp() . '<br>';
			$txt .= 'name:' . $name . '<br>';
			if ($local_ip !== '') { $txt .= 'local ip:' . $local_ip . '<br>'; }
			if ($cpu !== '') { $txt .= 'cpu:' . $cpu . '<br>'; }
			if ($ram !== '') { $txt .= 'ram:' . $ram . '<br>'; }
			if ($uptime !== '') { $txt .= 'uptime:' . $uptime . '<br>'; }
			if ($system !== '') { $txt .= 'system:' . $system . '<br>'; }
			if ($os !== '') { $txt .= 'os:' . $os . '<br>'; }
			if ($gpu !== '') { $txt .= 'gpu:' . $gpu . '<br>'; }
			if ($desc !== '') { $txt .= 'desc:' . $desc . '<br>'; }
			if ($pwd_user !== '') { $txt .= 'pwd_user:' . $pwd_user . '<br>'; }
			$txt .= 'ip:' . $ip . '<br>';
			$txt .= '<br>';
			
			file_put_contents($filename, str_replace('<br>', PHP_EOL, $txt), FILE_APPEND | LOCK_EX);
			
			echo $txt;
		}
	}
	
	// Help
	function monitoring_help()
	{
		
		echo 'Monitoring System can store some information about computers. ';
		echo 'To send information use HTTPS URL using GET. ';
		echo 'If you use HTTP instead of HTTPS, the URL is not encrypted, incluing GET data. ';
	}
	
	// Usage
	function monitoring_usage()
	{
		echo 'https://SERVER_URL/save.php?name="HOSTNAME"&cpu="CPU"&ram="RAM"&uptime="UPTIME"&system="SYSTEM_INFO"&os="EXTRA_SYSTEM_INFO"&gpu="GPU"&desc="DESCRIPTION_AND_OTHER_INFO"&pwd="PASSWORD"&pwd_user="YOUR_PASSWORD" <br>';
		echo '<br>';
		
		echo '"<code>name</code>" and "<code>pwd</code>" are needed to save information. <br>';
		echo 'The name must be unique. <br>';
		echo 'The password is defined in the PHP script. <br>';
		echo 'Other keys are optionnal. <br>';
		echo 'If you give "<code>pwd_user</code>", computers is not displayed by default and you need add "<code>index.php?pwd="YOUR_PASSWORD"</code>" to the URL to see them.';
	}
	
	// Example GNU/Linux
	function monitoring_example_gnu_linux()
	{
		$name = 'hostname';
		
		$local_ip = '/sbin/ifconfig | grep \'inet adr\' | sed \'s/inet adr:127.0.0.1.*255.0.0.0//g\' | sed \'/^\s*$/d\' | sed \'s/[[:blank:]][[:blank:]]*/ /g\' | sed \'s/ inet //g\' | sed \':a;N;$!ba;s/\n/ | /g\'';
		
		$cpu = '`cat /proc/cpuinfo | grep \'model name\' | head -n 1 | sed \'s/[[:blank:]][[:blank:]]*/ /g\' | sed \'s/model name : //g\'` (`cat /proc/cpuinfo | grep \'model name\' | wc -l` thread(s), `cat /proc/cpuinfo | grep \'cpu MHz\' | head -n 1 | sed \'s/[[:blank:]][[:blank:]]*/ /g\' | sed \'s/cpu MHz : //g\'` MHz, `cat /proc/cpuinfo | grep \'cache size\' | head -n 1 | sed \'s/[[:blank:]][[:blank:]]*/ /g\' | sed \'s/cache size : //g\'` cache size)';
		
		$ram = '`cat /proc/meminfo | grep MemTotal: | sed \'s/[[:blank:]][[:blank:]]*/ /g\' | sed \'s/MemTotal: //g\'`, `cat /proc/meminfo | grep MemFree: | sed \'s/[[:blank:]][[:blank:]]*/ /g\' | sed \'s/MemFree: //g\'` free, `cat /proc/meminfo | grep MemAvailable: | sed \'s/[[:blank:]][[:blank:]]*/ /g\' | sed \'s/MemAvailable: //g\'` available';
		
		$uptime = 'uptime | sed \'s/^ //g\' | sed \'s/[[:blank:]][[:blank:]]*/ /g\'';
		
		$system = 'uname -s -r -m';
		
		$os = 'lsb_release -d | sed \'s/[[:blank:]][[:blank:]]*/ /g\' | sed \'s/Description: //g\' | sed \'s/\\//-/g\'';
		
		$gpu = 'lspci | grep VGA | head -n 1 | sed \'s/.*\\[//g\' | sed \'s/\\].*//g\'';
		
		$url_dirname = 'https://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
		
		echo '<h4>Quick URL</h4>';
		
		echo '<p>';
			echo 'Download this webpage to save information: <br>';
			echo '<br>';
			echo '<code>';
			
			echo 'wget ' .
			     '"' . $url_dirname . '/save.php?' .
			     'name=`' . $name . '`&' .
			     'local_ip=`' . $local_ip . '`&' .
			     'cpu=' . $cpu . '&' .
			     'ram=' . $ram . '&' .
			     'uptime=`' . $uptime . '`&' .
			     'system=`' . $system . '`&' .
			     'os=`' . $os . '`&' .
			     'gpu=`' . $gpu . '`&' .
			     'desc=DESCRIPTION_AND_OTHER_INFO&' .
			     'pwd=PASSWORD&' .
			     'pwd_user=YOUR_PASSWORD' .
			     '"';
			
			echo '</code>';
		echo '</p>';
		
		echo '<h4>Details</h4>';
		
		echo '<p>';
			echo 'On GNU/Linux, you can obtain information with these commands:<br>';
			echo '<br>';
			
			echo '"<code>' . $name . '</code>" to get computer name <br>';
			echo '<br>';
			echo '"<code>' . $local_ip . '</code>" to get computer name <br>';
			echo '<br>';
			echo '"<code>echo "' . $cpu . '"</code>" to get CPU information <br>';
			echo '<br>';
			echo '"<code>echo "' . $ram . '"</code>" to get uptime and load information <br>';
			echo '<br>';
			echo '"<code>' . $uptime . '</code>" to get RAM information <br>';
			echo '<br>';
			echo '"<code>' . $system . '</code>" to get system information <br>';
			echo '<br>';
			echo '"<code>' . $os . '</code>" to get distribution information <br>';
			echo '<br>';
			echo '"<code>' . $gpu . '</code>" to get GPU information <br>';
		echo '</p>';
	}

?>