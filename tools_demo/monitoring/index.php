<!-- See README.txt -->


<?php require_once('./monitoring.php') ?>


<!DOCTYPE html>
<html lang="en">
	
	<?php $title = 'Monitoring System'; ?>
	
	<head>
		<meta charset="utf-8">
		<title><?php echo $title; ?></title>
	</head>
	
	<body>
		
		<h1><?php echo $title; ?></h1>
		
		<article>
		
			<h2>Computers in log file (example)</h2>
			
			<?php
				monitoring_log_display();
			?>
		
		</article>
		
		<article>
			
			<h2>Help</h2>
			
			<?php
				monitoring_help();
			?>
			
			<h3>Usage</h3>
			
			<?php
				monitoring_usage();
			?>
			
			<h3>Example on GNU/Linux</h3>
			
			<?php
				monitoring_example_gnu_linux();
			?>
			
		</article>
		
	</body>
	
</html>
