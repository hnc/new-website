# Archive 2021/11/17

This project is not maintained since 2016.

# new-website

Copyright © 2014, 2015 Lénaïc Bagnères, hnc@singularity.fr <br />
https://gitlab.com/hnc/new-website <br />
http://new-website.toile-libre.org/ <br />
http://hnc.toile-libre.org/index.php?section=dev&page=new-website <br />
https://www.lri.fr/~bagneres/index.php?section=dev&page=new-website

# new-website

new-website is a kind of simplified CMS

GNU Affero General Public License 3+

See http://new-website.toile-libre.org/
